<?php

interface Process_Callouts {


    /**
     * Promise of process
     * @param  Callouts $callout    Hipchat special text
     */
    public function process( Callouts $callout );

}
