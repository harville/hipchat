<?php


/**
 * Handle HipChat user input
 * @param  string $msg    User message
 * @return string         JSON-formatted output
 */
function hipchat_parser( $msg ) {

    $mentions = new Mentions( $msg );
    $emoticons = new Emoticons( $msg );
    $links = new Links( $msg );

    $output = array_merge( $mentions->formatted, $emoticons->formatted, $links->formatted );

    return ( $output ) ? json_encode( $output, JSON_UNESCAPED_SLASHES ) : false;

}


/**
 * Require all necessary classes
 */
function require_classes() {

    $classes = array(
        'callouts',
        'process-callouts',
        'validate-emotion',
        'request',
        'mentions',
        'emoticons',
        'links',
    );

    foreach ( $classes as $class ) {
        require_once 'class.' . $class . '.inc';
    }

}
require_classes();
