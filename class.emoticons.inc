<?php

class Emoticons extends Callouts {


    protected $_regex = '/(?<=\()[a-z]+(?=\))/';
    protected $_wrapper = 'emoticons';


    /**
     * Process the callout
     */
    public function process() {

        // Validate data
        $validate = new Validate_Emotion();
        $validate->process( $this );

    }

}
