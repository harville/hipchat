<?php

class Links extends Callouts {


    protected $_regex = '/(https?|ftp):\/\/[\d\w-.\/]+/';
    protected $_wrapper = 'links';


    /**
     * Process the callout
     */
    public function process() {

        // Get more info from the internet
        $request = new Request();
        $request->process( $this );

    }

}
