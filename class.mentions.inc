<?php

class Mentions extends Callouts {


    protected $_regex = '/(?<=@)[\w]+/';
    protected $_wrapper = 'mentions';


    /**
     * Process the callout
     */
    public function process() {

        // Currently a rubberstamp
        $this->processed = $this->parsed;

    }

}
