<?php

class Request implements Process_Callouts {


    /**
     * Parse a URL
     * Retrieve associated meta
     * @param  string $url     Parsed URL
     * @return object          URL and associated meta
     */
    private function _parse_link( $url ) {

        $file = @file_get_contents( $url );

        // bail if the request failed
        if ( false === $file ) return false;

        $regex = '/<title[^>]*>(.*)<\/title>/';
        $title = '';

        // grab link title
        if ( preg_match( $regex, $file, $match ) ) {
            $title = $match[1];
        }

        // package
        $link_meta = array(
            'url'   => $url,
            'title' => $title,
        );

        return (object) $link_meta;

    }


    /**
     * Request data from URLs
     * @param  Callouts $callout    Hipchat special text
     */
    public function process( Callouts $callout ) {

        foreach ( $callout->parsed as $url ) {

            $callout->processed[] = $this->_parse_link( $url );

        }

        // purge unverified links
        $callout->processed = array_filter( $callout->processed );

    }

}
