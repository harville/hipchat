<?php

class Validate_Emotion implements Process_Callouts {


    /**
     * Determine if the user is having a valid emotion
     * @param  string  $emotion    See: https://www.hipchat.com/emoticons
     * @return boolean             True if valid; Destroy human otherwise
     */
    private function _is_valid_emotion( $emotion ) {

        if ( strlen( $emotion ) > 15 ) return false;
        if ( ! mb_detect_encoding( $emotion, 'ASCII', true ) ) return false;

        return true;

    }


    /**
     * Validate emotions: ...where computers excel
     * @param  Callouts $callout    Hipchat special text
     */
    public function process( Callouts $callout ) {

        foreach ( $callout->parsed as $p ) {

            if ( $this->_is_valid_emotion( $p ) ) {
                $callout->processed[] = $p;
            }

        }

    }

}
