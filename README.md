##HipChat

###Requirements

Please write code (PHP) that takes a HipChat message string and returns a JSON string containing information about its contents. Special content to look for includes:

1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character (see http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work)
2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are ASCII strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon (see http://hipchat-emoticons.nyh.name)
3. Links - Any URLs contained in the message, along with the page's title

For example, calling your function with the following strings should result in the corresponding return values:

> `@chris you around?`

```json
{
    "mentions": [
        "chris"
    ]
}
```

> `Good morning! (megusta) (coffee)`

```json
{
    "emoticons": [
        "megusta",
        "coffee"
    ]
}
```

> `Olympics are starting soon http://www.nbcolympics.com`

```json
{
    "links": [
        {
            "url": "http://www.nbcolympics.com",
            "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
        }
    ]
}
```

> `@bob @john (success) such a cool feature https://twitter.com/jdorfman/status/430511497475670016`

```json
{
    "mentions": [
        "bob",
        "john"
    ],
    "emoticons": [
        "success"
    ],
    "links": [
        {
            "url": "https://twitter.com/jdorfman/status/430511497475670016",
            "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
        }
    ]
}
```