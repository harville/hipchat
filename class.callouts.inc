<?php

abstract class Callouts {


    // Variables
    public $msg;
    public $parsed = array();
    public $processed = array();
    public $formatted = array();
    protected $_regex;
    protected $_wrapper;

    // Promises
    abstract public function process();


    /**
     * Hipchat callout constructor
     * @param string $msg    User message
     */
    function __construct( $msg ) {

        $this->msg = $msg;

        $this->parse();
        $this->process();
        $this->format();

    }


    /**
     * Parse input
     */
    public function parse() {

        // find matches
        if ( preg_match_all( $this->_regex, $this->msg, $match ) ) {

            foreach ( $match[0] as $item ) {
                $this->parsed[] = $item;
            }

        }

    }


    /**
     * Format output
     */
    public function format() {

        $this->formatted = ( $this->processed ) ? array( $this->_wrapper => $this->processed ) : array();

    }

}
